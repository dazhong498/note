package compare;

import java.util.Comparator;

class EmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        if(o1.getName().compareTo(o2.getName()) != 0){
            return o1.getAge() - o2.getAge();
        }else{
            return o1.getName().compareTo(o2.getName());
        }
    }
}
