package compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Compare {
    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
//
//        list.add(10);
//        list.add(412);
//        list.add(14);
//        list.add(151);
//        list.add(13);
//        list.add(15);
//        list.add(235);
//
//        System.out.println(list);
//        Collections.sort(list, new MyComparator());
//        System.out.println(list);

//
//        List<Employee> employees = new ArrayList<>();
//
//        employees.add(new Employee(1, "David", 19,100f));
//        employees.add(new Employee(2, "Chen", 29,120f));
//        employees.add(new Employee(3, "David", 24,150f));
//
//        System.out.println(employees);
//        Collections.sort(employees);
//        System.out.println(employees);


        List<Student> students = new ArrayList<>();

        students.add(new Student(1, "David",44));
        students.add(new Student(4, "Jian",95));
        students.add(new Student(15, "Yash",45));
        students.add(new Student(5, "David",88));


//        for(Student student: students){
//            System.out.println(student);
//        }

        Collections.sort(students);
        for(Student student: students){
            System.out.println(student);
        }

    }
}
