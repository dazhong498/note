package core.accessmodifier.packOne;

public class B extends A{
    public static void main(String[] args) {
        A a = new A();
        a.defaultNumber = 1;
        a.protectedNumber = 1;
        a.publicNumber = 1;
    }
}
