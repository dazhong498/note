package core.accessmodifier.packOne;

public class A {
    public int publicNumber;
    protected int protectedNumber;
    private int privateNumber;
    int defaultNumber;

    public static void main(String[] args) {
        A a = new A();
        a.publicNumber = 1;
        a.protectedNumber = 1;
        a.defaultNumber = 1;
        a.privateNumber = 1;
    }
}
