package core.accessmodifier.packTwo;

import core.accessmodifier.packOne.A;

public class D extends A {
    public static void main(String[] args) {
        A a = new A();
        a.publicNumber = 1;

        D d = new D();
        d.protectedNumber = 1;
        d.publicNumber = 1;
    }
}
