package core.accessmodifier.packTwo;

import core.accessmodifier.packOne.A;

public class C {
    public static void main(String[] args) {
        A a = new A();
        a.publicNumber = 1;
    }
}
