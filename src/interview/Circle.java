package interview;

public class Circle extends Shape{

    static final double pi = 3.14;
    double radius;


    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    double Area() {
        return pi*this.radius;
    }
}
