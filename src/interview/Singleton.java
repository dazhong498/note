package interview;

public class Singleton {
    private int id;
    private String name;
    private static Singleton singleton;

    private Singleton(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Singleton getSingleton(int id, String name){
        if(singleton == null){
            singleton = new Singleton(id, name);
        }
        return singleton;
    }
}
