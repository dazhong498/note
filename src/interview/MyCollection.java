package interview;

import java.util.*;

public class MyCollection {
    public static void main(String[] args) {
//        better performance on searching
        List<Integer> al = new ArrayList<>();
//        better performance on adding and deleting
        List<Integer> ll = new LinkedList<>();


//        sorted set and map
        Set<Integer> ts = new TreeSet<>();
        ts.add(2);
        ts.add(5);
        ts.add(1);
        ts.add(6);
        System.out.println(ts);

        Map<Integer, String> tm = new TreeMap();
        tm.put(1, "me");
        tm.put(4, "you");
        tm.put(2, "him");
        tm.put(6, "her");
        System.out.println(tm);

//        random order of set and map
        Set<String> hs = new HashSet<>();
        hs.add("me");
        hs.add("You");
        hs.add("he");
        hs.add("One");
        hs.add("wo");
        hs.add("asd");
        System.out.println(hs);

        Map<Integer, String> hm = new HashMap();
        hm.put(100, "das");
        hm.put(-4, "aw");
        hm.put(444, "dasf");
        System.out.println(hm);

    }
}
