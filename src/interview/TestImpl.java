package interview;

public class TestImpl implements Test{
    @Override
    public void say() {
        System.out.println("Say Method");
    }

    public static void main(String[] args) {
        TestImpl test = new TestImpl();
        test.say();
        System.out.println();
    }
}
