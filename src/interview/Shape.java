package interview;

public abstract class Shape {
    String color;

    abstract double Area();
}
