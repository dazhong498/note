package interview;

public class Immute {
    final int id;
    final String name;

    public Immute(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Immute{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
