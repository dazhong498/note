package interview;

public class Class {
    private int id;
    private String name;
//    static private int number;
    public static int number;

    public Class(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public static void printClass(){
        System.out.println("Class Number" + Class.number);
    }

    public void print(){
        System.out.println("this is class");
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Class{" +
                "id=" + id +
                '}';
    }
}
