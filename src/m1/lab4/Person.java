package m1.lab4;

import m1.lab6.MyException;

public class Person {
    private String name;
    private float age;

    public Person(){

    }

    public Person(String name, float age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getAge() {
        return age;
    }

    public void setAge(float age) throws MyException {
        if(age <=15){
            throw new MyException("Person must be above 15 years old");
        }else{
            this.age = age;
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
