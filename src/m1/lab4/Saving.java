package m1.lab4;

public class Saving extends Account{
    private final double min;

    public Saving(double balance, Person accHolder, int min) {
        super(balance, accHolder);
        this.min = min;
    }

    @Override
    public boolean withdraw(double amount) {
        if(this.getBalance() - amount >= min){
            this.setBalance(this.getBalance() - amount);
            return true;
        }
        return  false;
    }
}
