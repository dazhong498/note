package m1.lab4;

public class CurrentAccount extends Account{
    private double overDraft;
    public CurrentAccount(double balance, Person accHolder, double overDraft) {
        super(balance, accHolder);
        this.overDraft = overDraft;
    }

    public boolean withdraw(double amount) {
        if(amount > overDraft){
            return false;
        }else{
            this.setBalance(this.getBalance() - amount);
           return  true;
        }
    }
}
