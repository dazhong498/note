package m1.lab4;

import m1.lab6.MyException;

public class Lab4 {
    public static void main(String[] args) {

        Account account = new Account(2000, new Person("Smith", 100));
        Account account1 = new Account(3000, new Person("Kathy", 100));

//        System.out.println(account);
//        System.out.println(account1);
//        account.deposit(2000);
//        account1.withdraw(2000);
//        System.out.println(account);
//        System.out.println(account1);

        Account account3 = new Saving(2000, new Person("Smith", 100), 1000);
        System.out.println(account3);
        account3.withdraw(1500);
        System.out.println(account3);
        account3.withdraw(500);
        System.out.println(account3);


        Person person = new Person();
        try {
            person.setAge(1);
        } catch (MyException e) {
            e.printStackTrace();
        }
        System.out.println(person);
    }
}
