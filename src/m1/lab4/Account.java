package m1.lab4;

import java.util.concurrent.atomic.AtomicInteger;

public class Account {
    private static AtomicInteger idGenerator = new AtomicInteger(100);

    private long accNum;
    private double balance;
    private Person accHolder;

    public Account(double balance, Person accHolder) {
        this.accNum = idGenerator.getAndIncrement();
        this.balance = balance;
        this.accHolder = accHolder;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    public boolean withdraw(double amount){
        this.balance -= amount;
        return true;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Person getAccHolder() {
        return accHolder;
    }

    public void setAccHolder(Person accHolder) {
        this.accHolder = accHolder;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accNum=" + accNum +
                ", balance=" + balance +
                ", accHolder=" + accHolder +
                '}';
    }
}
