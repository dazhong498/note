package m1.lab3;

//import m1.lab2.Gender;
import m1.lab2.Person;
import m1.lab6.MyException;

import java.sql.SQLOutput;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Lab3 {
    public static void main(String[] args) {
//        System.out.println("3.1");
//        threeOne();

//        System.out.println("3.2");
//        System.out.println(checkPositive());

//        System.out.println("3.3");
//        getDate();

//        System.out.println("3.4");
//        getLocalDateTime();

//        System.out.println("3.5");
//        getExprire();

//        System.out.println("3.6");
//        getZone();


        System.out.println("3.7");
        Person person = new Person("David", "Zhong");
        person.setDob();
        System.out.println(person.getAge());
        try {
            System.out.println(person.getFullName(person.getFirstName(), person.getLastName()));
        } catch (MyException e) {
            e.printStackTrace();
        }

    }

    public static void threeOne(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your string");
        String str = scanner.nextLine();

        System.out.println("Enter your option");
        int option = Integer.parseInt(scanner.nextLine());

        switch (option){
            case 1:
                System.out.println(addItself(str));
                break;
            case 2:
                System.out.println(oddProun(str));
                break;
            case 3:
                System.out.println(removeDuplicate(str));
                break;
            case 4:
                System.out.println(oddUpper(str));
                break;
        }
        scanner.close();
    }

    public static String addItself(String str){
        return str + str;
    }

    public static String oddProun(String str){
        char[] chars = str.toCharArray();
        for(int i = 1; i <= chars.length - 1; i += 2){
            chars[i] = '#';
        }
        return String.valueOf(chars);
    }

    public static String removeDuplicate(String str){
        char[] chars = str.toCharArray();
        Set<Character> charSet = new LinkedHashSet<Character>();
        for (char c : chars) {
            charSet.add(c);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for(Character c : charSet){
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    public static String oddUpper(String str){
        char[] chars = str.toCharArray();
        for(int i = 1; i <= chars.length - 1; i += 2){
            chars[i] = Character.toUpperCase(chars[i]);
        }
        return String.valueOf(chars);
    }

    public static boolean checkPositive(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a String");
        String str = scanner.nextLine();
        char[] chars = str.toCharArray();

        for(int i = 1; i <= chars.length - 1; i++){
            if(chars[i] - chars[i - 1] < 0){
                scanner.close();
                return false;
            }
        }
        scanner.close();
        return true;
    }

    public static void getDate(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Date ");

        String dateStr = scanner.next();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date=null;
        try {
            date = dateFormat.parse(dateStr);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Date date1 = new Date();
        System.out.println(date);
        System.out.println(date1);
        System.out.println("Date: " + Math.abs(date.getDate() - date1.getDate()));
        System.out.println("Month: " + Math.abs(date.getMonth() - date1.getMonth()));
        System.out.println("Year: " + Math.abs(date.getYear() - date1.getYear()));
        scanner.close();
    }

    private static void getLocalDateTime(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Date 1");
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M/d/yyyy");
        LocalDate date = LocalDate.parse(scanner.nextLine(), dateFormat);
        System.out.println("Date 2");
        LocalDate date1 = LocalDate.parse(scanner.nextLine(), dateFormat);
//        Period period = new Period(fromDateTime, toDateTime);

        System.out.println("Date: " + date.until( date1, ChronoUnit.DAYS));
        System.out.println("Month: " + date.until( date1, ChronoUnit.MONTHS));
        System.out.println("Year: " + date.until( date1, ChronoUnit.YEARS));
        scanner.close();
    }

    private static void getExprire(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Date 1");
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M/d/yyyy");
        LocalDate date = LocalDate.parse(scanner.nextLine(), dateFormat);
        System.out.println("Months");
        int month = Integer.parseInt(scanner.nextLine());
        System.out.println("Years");
        int year = Integer.parseInt(scanner.nextLine());
        System.out.println(date);
        date = date.plusMonths(month);
        date = date.plusYears(year);
        System.out.println(date);
        scanner.close();
    }

    private static void getZone(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Time Zone");
        Date date = new Date();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone(scanner.nextLine()));

        System.out.println("Date and time in Madrid: " + df.format(date));
        scanner.close();
    }
}
