package m1.lab8;

import java.io.*;

public class Lab8 {
    public static void main(String[] args) throws Exception{
        FileReader fr = new FileReader("C:\\Users\\david\\Desktop\\note\\NodePad\\src\\m1\\lab8\\abc.txt");

        int i;
        StringBuilder stringBuilder = new StringBuilder();
        while ((i=fr.read()) != -1) {
            stringBuilder.append((char) i);
        }

        stringBuilder.reverse();
        fr.close();

        FileWriter fw = new FileWriter("C:\\Users\\david\\Desktop\\note\\NodePad\\src\\m1\\lab8\\abc.txt", false);
        char[] chars = stringBuilder.toString().toCharArray();

        for(int j = 0; j <= chars.length - 1; j++){
            fw.write(chars[j]);
        }
        fw.close();


        BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\david\\Desktop\\note\\NodePad\\src\\m1\\lab8\\numberx.txt"));
        String str = br.readLine();
        String[] strings = str.split(",");
        for (String s: strings){
            if(Integer.parseInt(s) % 2 == 0){
                System.out.println(s);
            }
        }

        br.close();
    }
}
