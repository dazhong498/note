package m1.lab2;

import m1.lab6.MyException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Scanner;

enum Gender
{
    M, F;
}
public class Person {
    private String firstName;
    private String lastName;
    private Gender gender;
    private String phone;
    private LocalDate dob;


    public Person() {
    }

    public Person(String firstName) {
        this.firstName = firstName;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }



    public Person(String firstName, String lastName, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
    }

    public Person(String firstName, String lastName, Gender gender, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.phone = phone;
    }

    public void setDob(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("DoB");
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M/d/yyyy");
        this.dob = LocalDate.parse(scanner.nextLine(), dateFormat);
        scanner.close();
    }

    public int getAge(){
        LocalDate now = LocalDate.now();
        return (int)this.dob.until( now, ChronoUnit.YEARS);
    }

    public String getFullName(String a, String b) throws MyException {
        if(this.firstName.trim().length() == 0 && this.lastName.trim().length() == 0){
            throw new MyException("Empty first name and empty last name");
        }else{
            return this.firstName + " " + this.lastName;
        }
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person" + "\n" +
                "firstName= " + firstName + "\n" +
                "lastName= " + lastName + "\n" +
                "gender= " + gender +  "\n" +
                "phone number= " + phone;
    }
}
