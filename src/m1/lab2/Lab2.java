package m1.lab2;

import java.util.Scanner;

public class Lab2 {
    public static void main(String[] args) {
        System.out.println("2.1");

        System.out.println("Person Details");
        System.out.println("-------------------");
        System.out.println("First Name: David" );
        System.out.println("Last Name: Zhong");
        System.out.println("Gender: M");
        System.out.println("Age: 20");
        System.out.println("Weight: 100");

        System.out.println("2.2");

//        A a = A.staticFactoryMethod();
//        A a = new A();
//        a.getFactoryMethod()

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Input a number");
//        String number = scanner.nextLine();
//        double num = Double.parseDouble(number);
//        if(num > 0){
//            System.out.println("Positive");
//        }else{
//            System.out.println("Negative");
//        }
//        scanner.close();

        System.out.println("2.3");
        Person person = new Person("david", "zhong", Gender.M);
        System.out.println(person);

        System.out.println("2.4");
        Person person1 = new Person("david", "zhong", Gender.M, "123-123-1234");
        System.out.println(person1);

        System.out.println("2.5");
        System.out.println("Enum Done");
    }
}
