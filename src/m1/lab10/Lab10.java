package m1.lab10;

import m1.lab2.Person;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Lab10 {
    public static void main(String[] args) {
        Properties properties = new Properties();

        try {
            properties.load(Lab10.class.getResourceAsStream("/PersonProps.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String firstName = properties.getProperty("Person_FirstName");
        String lastName = properties.getProperty("Person_LastName");
        System.out.println(firstName);
        System.out.println(lastName);

        Person person = new Person(firstName, lastName);
        System.out.println(person);

        ConnectionUtil connectionUtil = null;
        Connection connection = null;
        try {
            connectionUtil = new ConnectionUtil("jdbc:postgresql://spark123.cbqsgrefosyg.us-east-2.rds.amazonaws.com:5432/testdb", "dazhong", "St620338.498", new org.postgresql.Driver());
            connection = connectionUtil.getConnection();
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sql = "Select * from account";
        Account account = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                account = new Account();
                account.setId(resultSet.getInt("accountNumber"));
                account.setName(resultSet.getString("holderName"));
                account.setBalance(resultSet.getFloat("balance"));
                account.setType(resultSet.getString("type"));
                System.out.println(account);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
