package m1.lab10;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    private String url;
    private String userName;
    private String userPassword;

    public ConnectionUtil(String url, String userName, String userPassword, Driver driver)throws SQLException {
        DriverManager.registerDriver(driver);

        this.url = url;
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public Connection getConnection()throws SQLException{
        return DriverManager.getConnection(this.url, this.userName, this.userPassword);
    }

}
