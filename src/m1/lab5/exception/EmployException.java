package m1.lab5.exception;

public class EmployException extends Exception{
    public EmployException(String message) {
        super(message);
    }
}
