package m1.lab5.service;

import m1.lab5.ConnectionUtil;
import m1.lab5.bean.Employee;

import java.util.List;
import java.util.Map;

public interface EmployeeService {
    Employee getEmployeeById(String name);
    void findInsurance(float salary, String designation);
    List<Employee> getEmployees();
    void addEmployee(Employee employee);
    void deleteEmployee(int id);
}
