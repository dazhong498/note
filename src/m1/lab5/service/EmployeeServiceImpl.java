package m1.lab5.service;

import m1.lab5.ConnectionUtil;
import m1.lab5.bean.Employee;
import m1.lab5.exception.EmployException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeServiceImpl implements EmployeeService{
    Map<String, Employee> employeeMap = new HashMap<>();
    private ConnectionUtil connectionUtil;


    public EmployeeServiceImpl(ConnectionUtil connectionUtil) {
        this.connectionUtil = connectionUtil;
    }

    @Override
    public Employee getEmployeeById(String name) {
//        return null;
        return employeeMap.get(name);
    }

    @Override
    public void findInsurance(float salary, String designation) {

    }

    @Override
    public List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();
        Employee temp = null;
        Connection connection = getConnection();
        String sql = "select * from emps";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                temp = new Employee();
                temp.setId(resultSet.getInt("id"));
                temp.setName(resultSet.getString("empname"));
                temp.setSalary(resultSet.getFloat("salary"));
                temp.setDesignation(resultSet.getString("designation"));
                temp.setDesignation(resultSet.getString("insurance"));
                employees.add(temp);
            }

            connection.commit();
            connection.close();
        }catch (SQLException | EmployException e){
            e.printStackTrace();
        }

        return employees;
    }

    public void deleteEmployee(int id){

        Connection connection = getConnection();
        String sql = "delete from emps where id = ?;";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();

            connection.commit();
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public void addEmployee(Employee employee) {
//        employeeMap.put(employee.getName(), employee);
        Connection connection = getConnection();
        String sql = "insert into emps(empname, salary, designation, insurance) values(?, ?, ?, ?);";
        try{
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setFloat(2, employee.getSalary());
            preparedStatement.setString(3, employee.getDesignation());
            preparedStatement.setString(4, employee.getInsuranceSecheme());

            preparedStatement.execute();

            connection.commit();
            connection.close();
        }catch (SQLException e){
            e.printStackTrace();
        }


    }


    private Connection getConnection(){
        Connection connection = null;
        try{
            connection = this.connectionUtil.getConnection();
            connection.setAutoCommit(false);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return connection;
    }
}
