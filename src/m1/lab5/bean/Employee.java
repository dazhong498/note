package m1.lab5.bean;

import m1.lab5.exception.EmployException;

public class Employee {
    private int id;
    private String name;
    private float salary;
    private String designation;
    private String insuranceSecheme;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) throws EmployException{
        if(salary < 3000){
            throw new EmployException("Salary should be above 3000");
        }else{
            this.salary = salary;
        }
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getInsuranceSecheme() {
        return insuranceSecheme;
    }

    public void setInsuranceSecheme(String insuranceSecheme) {
        this.insuranceSecheme = insuranceSecheme;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", designation='" + designation + '\'' +
                ", insuranceSecheme='" + insuranceSecheme + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
