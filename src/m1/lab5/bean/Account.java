package m1.lab5.bean;

import m1.lab2.Person;

public abstract class Account {
    private long accNum;
    private double balance;
    private Person accHolder;

    public Account(long accNum, double balance, Person accHolder) {
        this.accNum = accNum;
        this.balance = balance;
        this.accHolder = accHolder;
    }

    public void withdraw(double amount){

    }
}
