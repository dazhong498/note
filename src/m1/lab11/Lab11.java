package m1.lab11;

public class Lab11 {
    public static void main(String[] args) {
        String str = "abc";
        String str1 = "abc";
        String str2 = new String("abc");

        System.out.println(str.equals(str1));

        System.out.println(str.equals(str2));

        System.out.println(str == str1);
        System.out.println(str == str2);

    }
}
