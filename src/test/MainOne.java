package test;

public class MainOne {
    public static void main(String[] args) {
        String str = "Abcdefg";
        String str1 = "Abc  M";
        String str2 ="     ";
        str.length();
        str.trim();
        str.toCharArray();
        str.charAt(1);
        str.compareTo("Abc");

        str.split("b");
        str.toUpperCase();
        str.toLowerCase();
        System.out.println(str.trim());
        System.out.println(str1.trim());
        System.out.println(str2.trim().length() == 0);
        System.out.println(str.indexOf('A') == 0);

        System.out.println(str.substring(0, 3).equals("Abc"));
        System.out.println(str.replace('A', 'a').equals("abcdefg"));
        String str4 = str.join(", ", "David", "Zhong", "in", "the", "class");
        System.out.println(str4);
    }
}
