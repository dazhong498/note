package test;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class MyMain extends Thread{
    static ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();

    public void run(){
        map.put(10, "yash");
    }

    public static void main(String[] args)throws InterruptedException{
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(20);
        list.add(30);

        Iterator<Integer> integerIterator = list.iterator();
        while (integerIterator.hasNext()){
            Integer temp = integerIterator.next();
            System.out.println(temp);
//            list.add(4);
//            ConcurrentModificationException
        }

        MyMain myMain = new MyMain();

        myMain.start();

        map.put(2, "David");
        map.put(3, "David");
        map.put(4, "David");
        map.put(5, "David");
        map.put(6, "David");
        map.put(7, "David");
        map.put(8, "David");
        map.put(9, "David");
        map.put(41, "David");

        Iterator<Map.Entry<Integer, String>> iterator = map.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<Integer, String> entry = iterator.next();
            System.out.println(entry);
//            Thread.sleep(1000);
        }

        System.out.println(map);


//        list, set, map fail fast iterator
//        vector, hashTable, Stack,
//        happened on object level

//        concurrentHashMap, concurrentArrayList, concurrentArraySet are fail safe iterator,
//        happened on the segment level, block
//        create a clone of actual Collection and iterate over it




//        String str = "abc";



    }
}
