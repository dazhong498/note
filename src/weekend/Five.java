package weekend;


class Animal{
    public void sound(){
        System.out.println("Animal is making a sound");
    }
}

class Horse extends Animal{
//    override
    @Override
    public void sound(){
        System.out.println("Neigh");
    }

//    overload
    public void sound(int id){
        System.out.println("Neigh" + id);
    }
}
public class Five {
    public static void main(String[] args) {
        Animal m = new Horse();

        m.sound();
        Horse horse = new Horse();
        horse.sound(1);
    }
}
