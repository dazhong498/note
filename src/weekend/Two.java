package weekend;

public class Two {
    static final int num = 4;
    public static void main(String[] args) {
        final StringBuilder sb = new StringBuilder("Greek");

        System.out.println(sb);

        sb.append("For");

        System.out.println(sb);

//        cannot be reassign
//        num++;

//        final class cannot extends
//        final class A{
//
//        }
//        Compile error
//        class B extends class A{
//
//        }

//        Final method cannot override

//        class A{
//            final void m1(){
//
//            }
//        }
//
//        class B extends class A{
//            Error
//            void m1(){
//            }
//        }

//        Finalize is the method that the Garbage Collection always calls just before the deletion/destroying for Garbage Collection
    }
}
