package weekend;


abstract class Base {
    abstract void fun();
}
class Derived extends Base {
    void fun() { System.out.println("Derived fun() called"); }
}


//abstract class
public class Seven {
}
