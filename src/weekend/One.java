package weekend;



public class One {
    private int id;
//    static method, Any static member can be accessed before any objects of its class are created, and without reference to any object.
//    directly call other static methods
//    directly access static data
//    cannot refer to this or super in any way
    static void m1(){
        System.out.println("M1 Method");
    }

//    static variable, a single copy of variable is created and shared all the objects at class level
    static int a = 10;
    static int b;

//    static block, run it once when class is load
    static {
        b = a * 10;
    }

    public static void main(String[] args) {
        m1();
        System.out.println(b);

        One one = new One();
        one.id = 2;
        One.a = 3;
    }
}
