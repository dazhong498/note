package weekend;


import com.sun.org.apache.xpath.internal.axes.ContextNodeList;

import java.util.*;

//Collection
public class Eight {
    public static void main(String[] args) {
//        List interface allows duplicate objects
//        ArrayList has a good performance on searching
        List<Integer> arrayList = new ArrayList<>();
        listAdd(arrayList);
        System.out.println(arrayList);

//        LinkedList has a good performance on adding and deleting
        List<Integer> linkedList= new LinkedList<>();
        listAdd(linkedList);
        System.out.println(linkedList);

//        Set Interface does not allow duplicate objects
//        HashSet random order
//        LinkedHashSet follows the order as add
        Set<Integer> hashSet = new HashSet<>();
        setAdd(hashSet);
        System.out.println(hashSet);


        Set<Integer> treeSet = new TreeSet<>();
        setAdd(treeSet);
        System.out.println(treeSet);

//        Map interface has key-value pair

//        Set<Employee> employeeTreeSet = new TreeSet<>(new EmployeeComparator());
        Set<Employee> employeeTreeSet = new TreeSet<>();
        employeeTreeSet.add(new Employee(1, "David"));
        employeeTreeSet.add(new Employee(3, "Andy"));
        employeeTreeSet.add(new Employee(2, "Payal"));
        System.out.println(employeeTreeSet);






    }

    public static void listAdd(List<Integer> list){
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(10);
    }

    public static void setAdd(Set<Integer> set){
        set.add(10);
        set.add(40);
        set.add(50);
        set.add(60);
        set.add(20);
        set.add(30);
    }

    public static void mapAdd(Map<Integer, String> map){
        map.put(1, "One");
        map.put(2, "Two");
        map.put(5, "Three");
        map.put(3, "Four");
        map.put(6, "Five");
    }


//    public static void sortByName(Set<Employee> employees){
//        Collections.sort(employees);
//    }

}
